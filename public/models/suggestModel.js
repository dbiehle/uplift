'use strict';
const mongoose = require('mongoose');

// Suggestion data
var SuggestSchema = new mongoose.Schema({
  suggestions: String,
  date: { type: Date, default: Date.now },
  votes: {
    ups: { type: Number, default: 0 },
    downs: { type: Number, default: 0 }
  },
  saves: {type: Number, default: 0 }
});

module.exports = mongoose.model('Suggestion', SuggestSchema);
