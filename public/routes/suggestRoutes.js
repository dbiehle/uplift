'use strict';
module.exports = (app) => {
  const suggest = require('../controllers/suggestController');

  app.route('/suggest')
    .get(suggest.getLast5Suggestions)
    .post(suggest.createSuggestion);
}
