'use strict';

(function () {
  setBgGradientByHour();
  updateSuggestionsList();
})();

// Function to change the background gradient to match the sky for the time of day
function setBgGradientByHour(){
  let nowHour = new Date().getHours();
  if (nowHour >= 20 || nowHour < 5) {
    document.body.style.background = 'linear-gradient(hsl(275, 100%, 30%), hsl(240, 100%, 24%))';
  } else if (nowHour >= 5 && nowHour < 8) {
    document.body.style.background = 'linear-gradient(hsl(298, 100%, 50%), hsl(240, 100%, 24%))';
  } else if (nowHour >= 8 && nowHour < 16) {
    document.body.style.background = 'linear-gradient(hsl(62, 100%, 65%), hsl(210, 100%, 63%))';
  } else if (nowHour >= 16 && nowHour < 20) {
    document.body.style.background = 'linear-gradient(hsl(240, 100%, 50%), hsl(298, 100%, 50%))';
  }
}

function updateSuggestionsList() {
  fetch('/suggest', { method: 'GET'})
    .then(res => {
      if (!res.ok) {
        console.log(res);
        throw new Error('Trouble gettings Suggestions from /suggest endpoint');
      }
      return res.json();
    })
    .then(suggestions => {
      // first, empty list
      const listSuggestions = document.querySelector('.list-suggest');
      while(listSuggestions.firstChild) {
        listSuggestions.removeChild(listSuggestions.firstChild);
      }
      // then fill with last five suggestions
      if (suggestions.length) {
        suggestions.forEach(s => {
          var liAdd = document.createElement('li');
          liAdd.classList.add('list__item');
          var liText = document.createTextNode(s.suggestions);
          liAdd.appendChild(liText);
          listSuggestions.appendChild(liAdd);
        })
      }
    })
}

const postSuggestions = document.querySelector('.form-suggest');
postSuggestions.addEventListener('submit', event => {
  event.preventDefault();
  const suggestForm = document.forms.form_suggest;
  const suggestBody = { suggestions: suggestForm.elements.suggestions.value}
  fetch('/suggest', {
    method: 'POST',
    body: JSON.stringify(suggestBody),
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(res => {
      if (!res.ok) {
        console.log(res);
        throw new Error('Trouble gettings Suggestions from /suggest endpoint');
      }
      return res.json();
    })
    .then(() => {
      updateSuggestionsList();
    })
})
