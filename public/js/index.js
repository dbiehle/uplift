'use strict'

// Function to change the background gradient to match the sky for the time of day
//  added to body for onload event
function setBgGradientByHour(){
  let nowHour = new Date().getHours();
  if (nowHour >= 20 || nowHour < 5) {
    document.body.style.background = 'linear-gradient(hsl(275, 100%, 30%), hsl(240, 100%, 24%))';
  } else if (nowHour >= 5 && nowHour < 8) {
    document.body.style.background = 'linear-gradient(hsl(298, 100%, 50%), hsl(240, 100%, 24%))';
  } else if (nowHour >= 8 && nowHour < 16) {
    document.body.style.background = 'linear-gradient(hsl(62, 100%, 65%), hsl(210, 100%, 63%))';
  } else if (nowHour >= 16 && nowHour < 20) {
    document.body.style.background = 'linear-gradient(hsl(240, 100%, 50%), hsl(298, 100%, 50%))';
  }
}
