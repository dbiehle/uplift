'use strict';
const Suggest = require('../models/suggestModel');

module.exports.getLast5Suggestions = function(req, res) {
  Suggest.find({}, 'suggestions date', {$sort: {'date': -1}}, function(err, docs){
    if (err)
      res.send(err);
    res.json(docs.slice(0,5));
  });
};

module.exports.createSuggestion = function(req, res) {
  console.log('Inside of createSuggestion function...');
  console.log(req.body);
  // create a new suggestion from submitted form
  const newSuggestion = new Suggest({ suggestions: req.body.suggestions });
  // save that suggestion to db
  newSuggestion.save()
    .then(s => {
      console.log(`Added this uplifting suggestion "${s.suggestions}"`);
      return res.json(s);
    })
    .catch(err => {
      res.status(400).send(`Unable to save to database :( : ${err}`);
      console.error(err);
    })
}
