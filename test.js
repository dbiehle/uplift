'use strict';

const mongoose = require('mongoose');
require('dotenv').config();

let url = process.env.DB_URL;
mongoose.connect(url, { useNewUrlParser: true }, function(err, client){
  const db = client.db;
  db.collection('suggestions').find({}, function(err, docs) {
    if (err) {
      console.error(err);
    }
    if (!docs) {
      console.log('No documents found');
    }
    docs.forEach(function(el){
      console.log(el.date.toDateString() + ' | ' + el.suggestions);
    })
  });

  db.collection('reports').find({}, function(err, docs) {
    if (err) {
      console.error(err);
    }
    if (!docs) {
      console.log('No documents found');
    }
    docs.forEach(function(el){
      console.log(el.date.toDateString() + ' | ' + el.reports);
    })
  });

  client.close();
});
