# Uplift
## Suggest, perform, share acts of kindness
(a web and mobile app)
by Drew Biehle

Ways to interact with the app:
* Suggest a way to do something uplifting for someone else. Examples:
  - Smile and nod at 5 different people you don't know
  - Buy coffee for the next person in line
  - Give a grocery gift card to an unsheltered person
  - Play a game with an elderly person at a senior center.
* Read through the uplifting ideas.
* Read through reports of uplifting acts.
* Add an idea to your plans to uplift.
  - Attach a location to one of your planned ideas to invite others to participate.
  - Meet other uplifting people.
* Tell a story of something uplifting you did for someone else. Include photos and or videos.
