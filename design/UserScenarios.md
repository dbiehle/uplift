# User Scenarios

Martha has had a rough week and it's only Tuesday. If she could just read about something good in the world, it might be enough to float her through to the end of the week.

Drew, a married father of a busy 8 year-old, wants to do something positive for someone else, something that could offset all the negativity he sees. But he's not sure what that could be.

Drew (a different Drew, you don't know him, but he's just as handsome as that other one) has loads of ideas for ways to bring a little sunshine into other people's lives. Some big ideas, some small, but he needs a place to write them all down, if only to clear his head, and it would be great if he could share his ideas with the world.

Melanie just did something out of character. She saw a woman outside the grocery store with a sign that said “anything helps”. She went inside to buy groceries and also bought a gift card for $20 of groceries. Giving it to the woman outside made her feel so good that she wants to share that feeling with someone, but she doesn't want to brag to a friend. She wants to share the story anonymously.
